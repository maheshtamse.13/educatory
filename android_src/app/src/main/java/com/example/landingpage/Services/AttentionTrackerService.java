package com.example.landingpage.Services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.os.IBinder;

import androidx.annotation.Nullable;


public class AttentionTrackerService extends Service {

    private void stopService(){
        stopForeground(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createForegroundNotif();
    }

    private void createForegroundNotif() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {



        if(intent.getBooleanExtra("stopService",false))
            stopService();

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
