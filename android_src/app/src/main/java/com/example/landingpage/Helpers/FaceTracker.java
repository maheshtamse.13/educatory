package com.example.landingpage.Helpers;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.landingpage.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;


import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.concurrent.ExecutionException;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.FocusMode;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.result.Photo;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.result.WhenDoneListener;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.selector.LensPositionSelectorsKt;
import io.fotoapparat.selector.ResolutionSelectorsKt;
import io.fotoapparat.selector.SelectorsKt;
import io.fotoapparat.view.CameraView;

public class FaceTracker {

    private Context c;
    private Fotoapparat camera;
    public int sampleAt = 10;
    private boolean stopRunning = false;
    private Handler handler = new Handler();
    private CameraView cameraView;
    private Bitmap source;
    private FaceTrackerErrors callback= new FaceTrackerErrors() {
        @Override
        public void userPayingAttention() {

        }

        @Override
        public void userNotPayingAttention(boolean isFaceThere) {

        }
    };

    public void stop(){
        stopRunning = true;
    }

    public FaceTracker(Context c,CameraView cameraView, FaceTrackerErrors callback) {
        this.c = c;
        this.cameraView = cameraView;
        this.camera = getCameraInstance();
        if(callback!=null)
            this.callback = callback;
        startHandler();
    }

    private void startHandler() {

        Runnable r = new Runnable() {
            @Override
            public void run() {
                camera.start();
                cameraView.setVisibility(View.INVISIBLE);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(500);
                            PhotoResult photoResult = camera.takePicture();
                            extractDetails(photoResult);
                            camera.stop();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

                if(!stopRunning)
                    handler.postDelayed(this,sampleAt*1000);
            }
        };
        handler.post(r);

    }

    void showToast(final String text){
        Log.d("Face tracker",text);

//        handler.post(new Runnable() {
//            @Override
//            public void run() {
////                Toast.makeText(c, text, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    private void extractDetails(PhotoResult photoResult) {
        showToast("Captured Frame");
        try {
            final BitmapPhoto photo = photoResult.toBitmap().await();
            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            source = photo.bitmap;
            source =  Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
            handler.post(new Runnable() {
                @Override
                public void run() {
                    MainActivity.viewById1.setImageBitmap(source);
                }
            });
            showToast("await");
            Log.d("Image extracted",""+photo.bitmap.getByteCount());
            InputImage inputImage = InputImage.fromBitmap(source,0);
                final FaceDetector client = FaceDetection.getClient(buildFaceOptions());
                client.process(inputImage).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
                    @Override
                    public void onComplete(@NonNull Task<List<Face>> task) {
                        client.close();
                        if(!task.isComplete()){
                            showToast("Error in Ml lib");
                            return;
                        }
                        if(task.getException()!=null){
                            task.getException().printStackTrace();
                            Log.d("Extracted Img",task.getException().getLocalizedMessage());
                            return;
                        }
                        List<Face> result = task.getResult();
                        if(result!=null)
                            parseFaces(result);
                    }
                });
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void parseFaces(List<Face> result) {
        showToast("Faces found "+result.size());
        if(result.size() == 0){
            callback.userNotPayingAttention(false);
            return;
        }
        for (Face face : result) {
            if(face.getLandmark(FaceLandmark.LEFT_EYE) !=null
                    && face.getLandmark(FaceLandmark.RIGHT_EYE)!=null){
                if(face.getLeftEyeOpenProbability()!= null && face.getRightEyeOpenProbability()!=null)
                    if (face.getLeftEyeOpenProbability()+face.getRightEyeOpenProbability() < 1.0f)
                        continue;
                callback.userPayingAttention();
                return;
            }
        }
        callback.userNotPayingAttention(true);
    }

    private Fotoapparat getCameraInstance() {
        return Fotoapparat.with(c)
                .into(cameraView)
                .previewScaleType(ScaleType.CenterCrop)
                .photoResolution(ResolutionSelectorsKt.highestResolution())
                .focusMode(SelectorsKt.firstAvailable(
                        FocusModeSelectorsKt.continuousFocusPicture(),
                        FocusModeSelectorsKt.autoFocus(),
                        FocusModeSelectorsKt.fixed()
                ))
                .lensPosition(LensPositionSelectorsKt.front())
                .build();
//        f.start();
//        PhotoResult photoResult = f.takePicture();
//        photoResult.toBitmap();

    }


    FaceDetectorOptions buildFaceOptions(){

        return new FaceDetectorOptions.Builder()
                .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
                .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_NONE)
                .build();

    }

    public interface FaceTrackerErrors{
        public void userPayingAttention();
        public void userNotPayingAttention(boolean isFaceThere);
    }

}
