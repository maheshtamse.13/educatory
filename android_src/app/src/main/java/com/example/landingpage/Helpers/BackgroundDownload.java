package com.example.landingpage.Helpers;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;

public class BackgroundDownload {
    Context context;

    public BackgroundDownload(Context context) {
        this.context = context;
    }

    public void startDownload(String url,String path){

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Downloading some notes");
        request.setTitle("Educatory");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOCUMENTS,
                String.format("Educatory/%s",path));

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        if(manager!=null)
            manager.enqueue(request);
    }


}
