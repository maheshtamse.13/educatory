package com.example.landingpage;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.landingpage.Helpers.AppSwitchChecker;
import com.example.landingpage.Helpers.BackgroundDownload;
import com.example.landingpage.Helpers.FaceTracker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutionException;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.selector.LensPositionSelectorsKt;
import io.fotoapparat.selector.ResolutionSelectorsKt;
import io.fotoapparat.selector.SelectorsKt;
import io.fotoapparat.view.CameraView;


public class MainActivity extends AppCompatActivity {
    public static  ImageView viewById1 ;

    private AppBarConfiguration mAppBarConfiguration;

    void trial(){
        MainActivity.viewById1 = findViewById(R.id.imageView);
        final View viewById = findViewById(R.id.fotoapparat);
        CameraView cameraView = new CameraView(this);
        cameraView.setLayoutParams(viewById.getLayoutParams());
        final Fotoapparat f = Fotoapparat.with(MainActivity.this)
                .into(cameraView)
                .previewScaleType(ScaleType.CenterCrop)
                .photoResolution(ResolutionSelectorsKt.highestResolution())
                .focusMode(SelectorsKt.firstAvailable(
                        FocusModeSelectorsKt.autoFocus(),
                        FocusModeSelectorsKt.fixed()
                ))
                .lensPosition(LensPositionSelectorsKt.front())
                .build();
        f.start();
        viewById.setVisibility(View.INVISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//                Toast.makeText(MainActivity.this, "Trying bitmap", Toast.LENGTH_SHORT).show();

                PhotoResult photoResult = f.takePicture();
                try {
                    final BitmapPhoto await = photoResult.toBitmap().await();
                    Log.d("AttentionTracker","got bitmap");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            viewById1.setImageBitmap(await.bitmap);
                            viewById1.setVisibility(View.VISIBLE);
                            viewById.setVisibility(View.INVISIBLE);
                            Toast.makeText(MainActivity.this, "Image set", Toast.LENGTH_SHORT).show();

                        }
                    });
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
//                PhotoResult photoResult = f.takePicture();
//                try {
//                    BitmapPhoto await = photoResult.toBitmap().await();
//                    ImageView viewById1 = findViewById(R.id.imageView);
//                    viewById1.setImageBitmap(await.bitmap);
//                    Toast.makeText(MainActivity.this, "Image set", Toast.LENGTH_SHORT).show();
//                } catch (ExecutionException e) {
//                    e.printStackTrace();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                new FaceTracker(MainActivity.this);
    }

    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BackgroundDownload helper = new BackgroundDownload(MainActivity.this);
                helper.startDownload("https://firebasestorage.googleapis.com/v0/b/educatory-popcoders.appspot.com/o/submissions%2FsubmissionId%2Fsubmittied%2Fchatbot3.png?alt=media&token=1d201498-6384-4185-b178-eb7feb28872b",
                        "Chatbot");



//                MainActivity.viewById1 = findViewById(R.id.imageView);
//                new AppSwitchChecker(MainActivity.this, new AppSwitchChecker.AppSwitchErrors() {
//                    @Override
//                    public void appSwitchDetected(boolean isBlackListed) {
//                        if(toast!=null)
//                            toast.cancel();
//                        if(isBlackListed)
//                            toast = Toast.makeText(MainActivity.this,"Backlisted",Toast.LENGTH_SHORT);
//                        else
//                            toast = Toast.makeText(MainActivity.this,"Not blacklisted",Toast.LENGTH_SHORT);
//
//                        toast.show();
//                    }
//
//                    @Override
//                    public void mainAppInForeground() {
//                        if(toast!=null)
//                            toast.cancel();
//                        toast = Toast.makeText(MainActivity.this,"Paying attention",Toast.LENGTH_SHORT);
//                        toast.show();
//                    }
//                }).startRunning();
//                trial();
//                new FaceTracker(MainActivity.this, (CameraView) findViewById(R.id.fotoapparat), new FaceTracker.FaceTrackerErrors() {
//                    @Override
//                    public void userPayingAttention() {
//                        if(toast!=null)
//                            toast.cancel();
//                        toast = Toast.makeText(MainActivity.this,"Paying attention",Toast.LENGTH_SHORT);
//                        toast.show();
//                    }
//
//                    @Override
//                    public void userNotPayingAttention(boolean isFaceThere) {
//                        if(toast!=null)
//                            toast.cancel();
//                        if(isFaceThere)
//                            toast = Toast.makeText(MainActivity.this,"Look at screen",Toast.LENGTH_SHORT);
//                        else
//                            toast = Toast.makeText(MainActivity.this,"Come back to the phone",Toast.LENGTH_SHORT);
//
//                        toast.show();
//                    }
//                });
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return super.onSupportNavigateUp();
    }
}