package com.example.landingpage.Helpers;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class AppSwitchChecker {
    Context context;
    Handler handler;
    boolean shouldStop = false;
    int falseThreshAllowed = 5;
    int falseCount=falseThreshAllowed;
    int samplingRate = 1;
    String[] appPackages = new String[]{"com.whatsapp"};

    Set<String> blackList = new HashSet<>();

    AppSwitchErrors callback = new AppSwitchErrors() {
        @Override
        public void mainAppInForeground() {

        }

        @Override
        public void appSwitchDetected(boolean isBlackListed) {

        }
    };

    public AppSwitchChecker(Context context, AppSwitchErrors callback) {
        this.context = context;
        this.handler = new Handler();
        blackList.addAll(Arrays.asList(appPackages));
        if(callback != null)
            this.callback = callback;
    }

    public void startRunning(){
//        HandlerThread thread = new HandlerThread("BackgroundScanner",HandlerThread.NORM_PRIORITY);
        final Handler newHandler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String foregroundApp = getForegroundApp();
                if(foregroundApp!=null){
                    if(context.getPackageName().equals(foregroundApp)){
                        callback.mainAppInForeground();
                        falseCount = falseThreshAllowed;
                    }else{
                        falseCount-=1;
                        if(falseCount<0){
                            callback.appSwitchDetected(blackList.contains(foregroundApp));
                            falseCount = falseThreshAllowed;
                        }
                    }
                }else{
                    Log.d("app checker","did not get app");
                }
                    if(!shouldStop)
                        newHandler.postDelayed(this,samplingRate*1000);
            }
        };
        newHandler.post(r);

    }

    String getForegroundApp() {
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager usm = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,  time - 1000*1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    return mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                }
            }
        } else {
            ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
            return tasks.get(0).processName;
        }
        return null;
    }


    public interface AppSwitchErrors{
        public void appSwitchDetected(boolean isBlackListed);
        public void mainAppInForeground();
    }

}
