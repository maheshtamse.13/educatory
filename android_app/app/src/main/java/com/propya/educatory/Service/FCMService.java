package com.propya.educatory.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.propya.educatory.Helpers.BackgroundDownload;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class FCMService extends FirebaseMessagingService {
    public FCMService() {
    }

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        Log.d("New msg",data.toString());
        if(data.containsKey("type")){
            if(data.get("type").equals("download")){
                BackgroundDownload helper = new BackgroundDownload(this);
                helper.startDownload(data.get("url"),data.get("path"));
            }
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    public static void register(String s){

        String[] split = s.split("/");
        String base = "";

        for (String s1 : split) {
            base+=s1;
            Log.d("Sub",base);
            FirebaseMessaging.getInstance().subscribeToTopic(base);
            base+="_";
            FirebaseMessaging.getInstance().subscribeToTopic(base);
        }

    }



}
