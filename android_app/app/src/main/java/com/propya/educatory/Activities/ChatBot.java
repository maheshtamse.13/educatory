package com.propya.educatory.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Line;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.propya.educatory.Helpers.ActivityHelper;
import com.propya.educatory.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatBot extends ActivityHelper {

    ArrayList<DataSnapshot> snapshots = new ArrayList<>();
    ChatBotAdapter adapter;
    RecyclerView chatRecycler;
    DatabaseReference ref;


    @Override
    protected void viewReady(View v) {
        chatRecycler = findViewById(R.id.chatRecycler);
        adapter = new ChatBotAdapter(this,snapshots);

        ref = FirebaseDatabase.getInstance().getReference("chatBot").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                snapshots.clear();
                for (DataSnapshot child : snapshot.getChildren()) {
                    snapshots.add(child);
                }
                adapter.notifyDataSetChanged();
                chatRecycler.scrollToPosition(snapshots.size() - 1);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setReverseLayout(true);
//        linearLayoutManager.setStackFromEnd(true);
        chatRecycler.setLayoutManager(linearLayoutManager);
        chatRecycler.setAdapter(adapter);
    }

    @Override
    protected int getRootView() {
        return R.layout.activity_chat_bot;
    }

    public void sendMsg(View view) {
        EditText message = (EditText) ((LinearLayout) view.getParent()).getChildAt(0);
        String s = message.getText().toString();
        message.setText("");
        String key = ref.push().getKey();

        HashMap<String,Object> data = new HashMap<>();

        data.put("userMsg",true);
        data.put("text",s);

        ref.child(key).setValue(data);
    }


    class ChatBotAdapter extends RecyclerView.Adapter<ChatBotView>{

        Context context;
        ArrayList<DataSnapshot> messages;

        public ChatBotAdapter(Context context, ArrayList<DataSnapshot> messages) {
            this.context = context;
            this.messages = messages;
        }

        @NonNull
        @Override
        public ChatBotView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ChatBotView(LayoutInflater.from(context).inflate(R.layout.view_holder_chat_message,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull ChatBotView holder, int position) {
            DataSnapshot dataSnapshot = messages.get(position);
            String text = dataSnapshot.child("text").getValue(String.class);
            if(dataSnapshot.child("userMsg").getValue(Boolean.class)){
//                userMsg
                holder.userText.setText(text);
                holder.userText.setVisibility(View.VISIBLE);
                holder.botRoot.setVisibility(View.GONE);

            }else{
//                bot msg
                holder.userText.setVisibility(View.GONE);
                holder.botRoot.setVisibility(View.VISIBLE);
                holder.botButton.setVisibility(View.GONE);

                holder.botText.setText(text);
                if(dataSnapshot.child("deepLink").exists()){
                    holder.botButton.setVisibility(View.VISIBLE);
                    holder.botButton.setOnClickListener((v)->((ActivityHelper)context)
                            .startActivity(dataSnapshot.child("deepLink").getValue(String.class)));
                    holder.botButton.setText(dataSnapshot.child("buttonText").getValue(String.class));
                }

            }

        }

        @Override
        public int getItemCount() {
            return messages.size();
        }
    }


    class ChatBotView extends RecyclerView.ViewHolder{

        TextView userText,botText;
        LinearLayout botRoot;
        Button botButton;

        public ChatBotView(@NonNull View itemView) {
            super(itemView);
            userText = itemView.findViewById(R.id.userMsgText);
            botText = itemView.findViewById(R.id.botText);
            botButton = itemView.findViewById(R.id.botButton);
            botRoot = itemView.findViewById(R.id.botMain);
        }
    }

}