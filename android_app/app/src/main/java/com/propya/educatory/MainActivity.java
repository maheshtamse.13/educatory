package com.propya.educatory;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.propya.educatory.Activities.VideoCalling;
import com.propya.educatory.Helpers.ActivityHelper;
import com.propya.educatory.Helpers.BackgroundDownload;
import com.propya.educatory.Service.FCMService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends ActivityHelper {
@BindView(R.id.center_linear)
LinearLayout rootLinear;

    void login(){
        if(FirebaseAuth.getInstance().getCurrentUser() !=null)
            return;

        List<AuthUI.IdpConfig> providers = Collections.singletonList(
                new AuthUI.IdpConfig.GoogleBuilder().build());

        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                58);
    }

    @Override
    protected void viewReady(View v) {
        login();
        BackgroundDownload helper = new BackgroundDownload(this);
        helper.listDir(null);

        FCMService.register("SPIT/9th/Batch B");


        Intent videoCalling = new Intent(this, VideoCalling.class);
        videoCalling.putExtra("lectureID", "imarandomidreplaceme");
        addFeature(VideoCalling.class.getSimpleName(), videoCalling);
    }

    @Override
    protected int getRootView() {
        return R.layout.activity_main;
    }




    void addFeature(Class c){
        addFeature(c.getSimpleName(),new Intent(this,c));
    }
    void addFeature(String name, Intent i){
        addFeature(name, v -> startActivity(i));
    }

    void addFeature(String name, View.OnClickListener i){
        Button b = new Button(this);
        b.setText(name);
        b.setOnClickListener(i);
        rootLinear.addView(b);
    }

}