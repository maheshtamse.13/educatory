package com.propya.educatory.Helpers;

import android.content.Context;

import io.fotoapparat.view.CameraView;

public class AttentionTracker implements AppSwitchChecker.AppSwitchErrors, FaceTracker.FaceTrackerErrors {

    AttentionCallback callback;
    Context c;
    AppSwitchChecker checker;
    FaceTracker faceTracker;


    public AttentionTracker(Context c, AttentionCallback callback, CameraView cameraView) {
        this.c = c;
        this.callback = callback;
        checker = new AppSwitchChecker(c,this);
        faceTracker = new FaceTracker(c, cameraView, this);
    }


    public void startTracking(){
        faceTracker.start();
        checker.startRunning();
    }

    public void stopTracking(){
        checker.stopRunning();
        faceTracker.stop();
    }

    @Override
    public void appSwitchDetected(boolean isBlackListed) {

    }

    @Override
    public void mainAppInForeground() {

    }

    @Override
    public void userPayingAttention() {

    }

    @Override
    public void userNotPayingAttention(boolean isFaceThere) {

    }


    public interface AttentionCallback{
        public void attentionDetected(boolean payingAttention, boolean switchedApp, boolean notLooking);
    }



}
