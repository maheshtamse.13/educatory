package com.propya.educatory.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.propya.educatory.Helpers.ActivityHelper;
import com.propya.educatory.R;

import butterknife.BindView;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;

public class VideoCalling extends ActivityHelper {
    @BindView(R.id.activity_video_chat_view)
    RelativeLayout rootview;
    @BindView(R.id.remote_video_view_container) RelativeLayout remoteViewContainer;
    @BindView(R.id.icon_padding) RelativeLayout iconpadding;
    @BindView(R.id.local_video_view_container)
    FrameLayout localVideoviewcontainer;
    @BindView(R.id.control_panel) RelativeLayout controlPanel;
    @BindView(R.id.btn_call)
    ImageView callButton;
    @BindView(R.id.btn_switch_camera)
    ImageView switchCameraButton;
    @BindView(R.id.btn_mute)
    ImageView muteButton;
    private static final int PERMISSION_REQ_ID = 22;
    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };



    private RtcEngine mRtcEngine;
    private boolean mMuted=false;
    private boolean mCallEnd=false;


    private String teleToken;
    private String channelName;


    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {
        @Override
        public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("agora","Join channel success, uid: " + (uid & 0xFFFFFFFFL));
                }
            });

            //       hide progress dialog for joining
            VideoCalling.super.stopProgress();
        }


        @Override
        public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("agora","First remote video decoded, uid: " + (uid & 0xFFFFFFFFL));
                    setupRemoteVideo(uid);
                }
            });
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i("agora","User offline, uid: " + (uid & 0xFFFFFFFFL));
//                    onRemoteUserLeft();
                }
            });
        }
    };



    private void onRemoteUserLeft() {

        leaveChannel();
    }
    @Override
    protected int getRootView() {

        //TODO CONNECT LAYOUT FILES HERE
        return R.layout.activity_video_calling;
    }

    //Button action handlers
    public void onCallClicked(View view) {
        leaveChannel();
        finish();
    }

    public void onLocalAudioMuteClicked(View view) {
        mMuted = !mMuted;
        if(mMuted)
        {
            muteButton.setImageResource(R.drawable.ic_mic_off_white_24dp);
        }
        else
        {
            muteButton.setImageResource(R.drawable.ic__mic__white_24dp);
        }
        mRtcEngine.muteLocalAudioStream(mMuted);
    }
    public void onSwitchCameraClicked(View view) {
        mRtcEngine.switchCamera();
    }



    @Override
    protected void viewReady(View v) {
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[2], PERMISSION_REQ_ID)) {
//            initEngineAndJoinChannel();

        }
        teleToken= getIntent().getStringExtra("teleToken");
        channelName=getIntent().getStringExtra("lectureID");
        if (teleToken==null || channelName == null)
        {
            channelName="testcahnnelforeducatory";
            teleToken=null;

        }
        startProgress(new String [] {"Joining Channel","Please wait a while"});
        initializeEngine();
    }



    // Initialize the RtcEngine object.
    private void initializeEngine() {

        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
            setupLocalVideo();

        } catch (Exception e) {
            Log.e("VideoLectures", Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }
//Event Handlers




    private void setupLocalVideo() {

        // Enable the video module.
        mRtcEngine.enableVideo();

        // Create a SurfaceView object.
//        private FrameLayout mLocalContainer;
        SurfaceView mLocalView;

        mLocalView = RtcEngine.CreateRendererView(getBaseContext());
        mLocalView.setZOrderMediaOverlay(true);
        localVideoviewcontainer.addView(mLocalView);
        // Set the local video view.
        VideoCanvas localVideoCanvas = new VideoCanvas(mLocalView, VideoCanvas.RENDER_MODE_HIDDEN, 0);
        mRtcEngine.setupLocalVideo(localVideoCanvas);
        joinChannel();
    }

    private void setupRemoteVideo(int uid) {

        // Create a SurfaceView object.
//        private RelativeLayout mRemoteContainer;
        SurfaceView mRemoteView;


        mRemoteView = RtcEngine.CreateRendererView(getBaseContext());
        remoteViewContainer.addView(mRemoteView);
        // Set the remote video view.
        mRtcEngine.setupRemoteVideo(new VideoCanvas(mRemoteView, VideoCanvas.RENDER_MODE_HIDDEN, uid));

    }

    private void joinChannel() {

        // Join a channel with a token.
        //TODO CHANGE THIS TOKEN AND CHANNEL
        mRtcEngine.joinChannel(teleToken, channelName, "Extra Optional Data", 0);
    }

    private void leaveChannel() {
        // Leave the current channel.
        mRtcEngine.leaveChannel();
    }

    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }

        return true;
    }






    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mCallEnd) {
            leaveChannel();
        }
        RtcEngine.destroy();
    }

}