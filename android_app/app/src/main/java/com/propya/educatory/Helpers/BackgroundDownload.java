package com.propya.educatory.Helpers;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

public class BackgroundDownload {
    Context context;

    public BackgroundDownload(Context context) {
        this.context = context;
    }

    public void startDownload(String url,String path){

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("Downloading some notes");
        request.setTitle("Educatory");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOCUMENTS,
                String.format("Educatory/%s",path));

        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        if(manager!=null)
            manager.enqueue(request);
    }

    public ArrayList<LectureFile> listDir(String dir){
        ArrayList<LectureFile> notes = new ArrayList<>();

        Log.d("File","listing");
        Log.d("File",Environment.getExternalStorageDirectory().toString());
        File external = new File(Environment.getExternalStorageDirectory()+"/Documents/Educatory");
        if(dir!=null)
            external = new File(external + dir);

        if(external.listFiles()==null)
            return notes;

        for (File file : external.listFiles()) {
            notes.add(new LectureFile(file));
        }

        return notes;
    }


    public class LectureFile{
        File f;


        public LectureFile(File f){
            this.f = f;
        }


    }


}
