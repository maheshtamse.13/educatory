console.log("This works")
google.charts.load('current', {'packages':['corechart','bar','table']});
google.charts.setOnLoadCallback(drawFees);
google.charts.setOnLoadCallback(drawAttendance);

function drawFees(){

    var data = google.visualization.arrayToDataTable([
        ['Amount', 'Paid', 'Unpaid' ],
        ['2010', 10, 24],
        ['2020', 16, 22],
        ['2030', 28, 19]
      ]);

      var options = {
        width: 600,
        height: 400,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '100%' },
        isStacked: true
      };
      

      var chart = new google.charts.Bar(document.getElementById('barchartFees'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
}

function drawAttendance(){

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Name');
    data.addColumn('number', 'Salary');
    data.addColumn('boolean', 'Full Time Employee');
    data.addRows([
      ['Mike',  {v: 10000, f: '$10,000'}, true],
      ['Jim',   {v:8000,   f: '$8,000'},  false],
      ['Alice', {v: 12500, f: '$12,500'}, true],
      ['Bob',   {v: 7000,  f: '$7,000'},  true]
    ]);

    var table = new google.visualization.Table(document.getElementById('attendanceChart'));

    table.draw(data, {showRowNumber: true, width: '100%', height: '100%' });
}
