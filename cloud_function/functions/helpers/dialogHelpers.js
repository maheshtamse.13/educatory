const functions = require("firebase-functions")
const admin = require("firebase-admin")
const serviceAccount = require("../serviceKey.json")

const dialogflow = require("dialogflow")
const dialogFlowFullFil = require("dialogflow-fulfillment")

const newMessageTrigger = functions.database.ref("chatBot/{userId}/{msgId}")
    .onCreate(async (snap,cont)=>{

        const data = snap.val();

        if(data.userMsg === undefined || !data.userMsg)
            return

        const sessionId = cont.params.userId;

        const sessionClient = new dialogflow.SessionsClient()

        const session = sessionClient.sessionPath("educatory-popcoders",sessionId)

        const responses = await sessionClient.detectIntent(
            {   session,
                queryInput:{
                    "text":{
                        "text":data.text,
                        "languageCode": "en-US"
                    }
                }
            })

        await addMessageUser(sessionId,responses[0].queryResult.fulfillmentText,undefined,undefined)

});

const answerMyQn = (userId,qn)=>{
    return Promise.resolve();
}

const getMyLectures = async (userId)=>{
    await addMessageUser(userId,"Give me a second while I fetch your lectures...",null,null);
    let userSnapshot = await admin.firestore().doc(`users/${userId}`).get();
    if(userSnapshot.data() === undefined ||  userSnapshot.data().level === undefined){
        await addMessageUser(userId,"No lectures found",null,null);
        return
    }
    const levels = userSnapshot.data().level.split();

    let currentLevel = "";
    const levelSearch = []
    for(let i=0 ; i <levels.length; i++){
        currentLevel+=levels[i];
        levelSearch.push(currentLevel);
    }
     let lectures = await admin.firestore()
         .collection("lectures")
         .where("level","in",levelSearch).orderBy("startTime").get();

    if(lectures.empty){
        await addMessageUser(userId,"No lectures found",null,null);
        return
    }
    let firstLec = null;
    let lecturePrint = `You have ${lectures.size} lecs\n`
    lectures.forEach(lec=>{
        if(firstLec==null){
            firstLec = lec.id;
        }
        let date = lec.data().startTime.toDate();

        let datePrint = `${date.getDate()} @ ${date.getHours()}:${date.getMinutes()}`


        // const format = new DateFormat('hh:mm a');

        lecturePrint += `${lec.data().subject} > ${ datePrint}\n`
    })

    console.log(lecturePrint);
    await addMessageUser(userId,lecturePrint,`http://www.educatory.in/lecture/${firstLec}`,"Join Lecture")


}


const finalFulfillment = functions.https.onRequest(async (request,response)=>{

    let session = request.body.session.split("/");
    const sessionId = session[session.length - 1]

    let agent = new dialogFlowFullFil.WebhookClient({request,response});


    async function searchMyQuery(a){
        await answerMyQn(sessionId,request.body.queryResult.parameters.query);
        // a.add("Search my query")
    }

    async function getLectures(a){
        await getMyLectures(sessionId);
        // a.add("get my schedule")
    }


    let intentMap = new Map();
    intentMap.set("MyQuery",searchMyQuery);
    intentMap.set("Lectures",getLectures);

    await agent.handleRequest(intentMap);
    response.status(200);
    response.end();
})



const addMessageUser = (user,msg,link,linkTitle)=>{
    let messageData = {
        "userMsg":false,
        "text":msg
    }
    if (link!==null && link!== undefined){

        messageData["deepLink"] = link;
        messageData["buttonText"] = linkTitle;
    }

    return admin.database().ref(`chatBot/${user}`).push(messageData)

}



const trial = async ()=>{
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL:"https://educatory-popcoders.firebaseio.com"
    })

    getMyLectures("ZL8puM73iVhUJdrGqkaL05DcJDw2")
    //
    // const sessionId = "sasa";
    //
    // const sessionClient = new dialogflow.SessionsClient({
    //     credentials : serviceAccount
    // })
    //
    // const session = sessionClient.sessionPath("educatory-popcoders",sessionId)
    //
    // const responses = await sessionClient.detectIntent(
    //     {   session,
    //         queryInput:{
    //             "text":{
    //                 "text":"I have a doubt",
    //                 "languageCode": "en-US"
    //             }
    //         }
    //     })
    // console.log(responses[0].queryResult.fulfillmentText)
}

if(module.parent===null){
    // trial()
}

module.exports = {
    "triggerNew":newMessageTrigger,
    finalFulfillment
}
