const functions = require("firebase-functions")
const admin = require("firebase-admin")


const newNotes = (obj)=> {

    const filePath = obj.name;
    const fileUrl = `https://firebasestorage.googleapis.com/v0/b/` +
        `${obj.bucket}/o/${encodeURIComponent(obj.name)}?alt=media&token=${obj.metadata.firebaseStorageDownloadTokens}`

    const paths = filePath.split("/")

    let path = "";

    for(let i =0;i< (paths.length-1);i++){
        path+= (paths[i]+"/")
    }
    path = path.substring(0,path.length-1);


    return admin.firestore().doc(path).get().then((doc)=>{
        let documentData = doc.data();
        console.log("data fetched ",documentData)
        let topicName = String(documentData.level);
        while (topicName.indexOf("/")!==-1){
            topicName = topicName.replace("/","_")
        }
        let promises = Array();
        let devicePath = `lectures/${documentData.subject}/${documentData.displayName}/${paths[paths.length-1]}`;

        promises.push(
            admin.firestore().doc(path).update({
                "attachments":admin.firestore.FieldValue.arrayUnion(fileUrl)
            })
        )

        promises.push(admin.messaging().sendToTopic(topicName,{
            data:{
                "type":"download",
                "url":fileUrl,
                "path":devicePath
            }
        }))

        return Promise.all(promises);
    })




};

module.exports = {
    newNotes
}