const functions = require("firebase-functions")
const admin = require("firebase-admin")

const lectures = require("./lectures")

const newSubmission = functions.storage.object().onFinalize((obj)=>{


    const filePath = obj.name;

    if(filePath.indexOf(".")===-1)
        return Promise.resolve();


    const fileUrl = `https://firebasestorage.googleapis.com/v0/b/` +
        `${obj.bucket}/o/${encodeURIComponent(obj.name)}?alt=media&token=${obj.metadata.firebaseStorageDownloadTokens}`

    const paths = filePath.split("/");

    const allowedPaths = ["submissions"]

    if(paths[0] === "lectures"){
        return lectures.newNotes(obj)
    }

    if(!allowedPaths.includes(paths[0])){
        return Promise.resolve();
    }
    return admin.firestore().doc(filePath.split(".")[0]).set({
        "attachments": [fileUrl],
        "createdAt": admin.firestore.FieldValue.serverTimestamp()
    })
})

const updateSubmissionPoints = functions.firestore.document("submissions/{submissionId}/submitted/{docId}")
    .onCreate(async (document,context)=>{
        const userId = context.params.docId.split("_")[0]

        let documentSnapshot = await admin.firestore().doc(`students/${userId}`).get();

        if(!documentSnapshot.exists)
            return

        let submissionDetails = await admin.firestore().doc(`submissions/${context.params.submissionId}`).get();

        if(submissionDetails.data().deadline > documentSnapshot.data().createdAt){
            await admin.firestore().doc(`students/${userId}`).set({
                "points": (documentSnapshot.data().points || 0) +  (submissionDetails.data().awardPoints || 20)
            },{merge:true})
        }

        await admin.firestore()
            .doc(`submission/${context.params.submissionId}/submitted/${context.params.docId}`)
            .set({
                "userId":userId,
                "userName":documentSnapshot.data().userName
            },{ merge : true})

        await admin.firestore().doc(`submissions/${context.params.submissionId}`).set({
            "submissionCount": (submissionDetails.data().submissionCount||0)+1
        },{ merge : true })

    });

module.exports = {
    newSubmission,
    updateSubmissionPoints
}